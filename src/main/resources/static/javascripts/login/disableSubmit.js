function disableSubmit(event, form) {
    form.login.disabled = true;
    form.login.innerHTML = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>';
}
