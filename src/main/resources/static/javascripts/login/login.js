function encrypt() {
    var password = $("#password").val();
    password = md5(password);
    return password;
}

function buildParam() {
    var password = encrypt();
    var username = $("#username").val();
    var data = new Object;
    data.username = username;
    data.password = password;
    return data;
}

function login(_bankName) {
    var data = buildParam();
    $.ajax({
        type: "POST",
        url: "/login-" + _bankName,
        data: data,
        dataType: "json",
        success: function(response) {
            window.location.href = response;
        }
    })
}