function openLink(server, token, url) {
    var w = 850;
    var h = 800;
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    var target = server + "?accessToken=" + token + "&redirect_url=" + url;
    console.log(target);
    return window.open(target, "Money Save - Connect Your Bank Account", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
};