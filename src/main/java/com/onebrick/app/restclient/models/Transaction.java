package com.onebrick.app.restclient.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import java.sql.Timestamp;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction {
    @JsonProperty("id")
    private long id;

    @JsonProperty("account_id")
    private String accountId;

    @JsonProperty("category_id")
    private long categoryId;

    @JsonProperty("subcategory_id")
    private long subcategoryId;

    @JsonProperty("merchant_id")
    private long merchantId;

    @JsonProperty("location_country_id")
    private long countryId;

    @JsonProperty("location_city_id")
    private long cityId;

    @JsonProperty("outlet_outlet_id")
    private long outletId;

    @JsonProperty("amount")
    private Double amount;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonProperty("date")
    private Timestamp date;

    @JsonProperty("description")
    private String description;

    @JsonProperty("status")
    private String status;

    @JsonProperty("direction")
    private String direction;
}
