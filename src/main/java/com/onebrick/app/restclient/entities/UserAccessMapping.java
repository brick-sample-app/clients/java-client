package com.onebrick.app.restclient.entities;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;

@Entity
@Table(name = "user_access_mapping")
@Getter
@Setter
public class UserAccessMapping {
    @Id
    @GeneratedValue
    private int id;

    @Column(name = "user_access_token")
    private String userAccessToken;

    @Column(name = "session_id")
    private String sessionId;

    @Column(name = "is_jenius")
    private boolean isJenius;

    @Lob
    @Column(name = "transactions", length = 100000)
    private String transactions;

    @Lob
    @Column(name = "accounts", length = 100000)
    private String accounts;
}
