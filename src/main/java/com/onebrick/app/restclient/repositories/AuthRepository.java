package com.onebrick.app.restclient.repositories;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Service;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class AuthRepository {

    private static final String BASE_URL = "http://dev.onebrick.asia/";
    //private static final String BASE_URL = "http://localhost:8080/";
    private static final String CLIENT_ID = "client_id_1";
    private static final String CLIENT_SECRET = "client_secret_1";
    private static final ObjectMapper MAPPER = new ObjectMapper();

    public JsonNode getPublicToken() throws Exception {
        JsonNode result = MAPPER.createObjectNode();
        try {
            var client = HttpClientBuilder.create().build();
            var request = new HttpGet(BASE_URL + "/v1/auth/token");
            var authHeader = CLIENT_ID + ":" + CLIENT_SECRET;
            var encHeader = Base64.encodeBase64(authHeader.getBytes(StandardCharsets.ISO_8859_1));
            authHeader ="Basic " + new String(encHeader);
            request.setHeader("Authorization", authHeader);
            var response = client.execute(request);
            var node = MAPPER.readValue(response.getEntity().getContent(), ObjectNode.class);
            if(node.has("data")) {
                result = node.get("data");
            }
        } catch (Exception e) {
            throw new Exception(e);
        }
        return result;
    }
}
