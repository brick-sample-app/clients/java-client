package com.onebrick.app.restclient.repositories;

import com.onebrick.app.restclient.entities.UserAccessMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccessMappingRepository extends JpaRepository<UserAccessMapping, Integer> {
    UserAccessMapping findBySessionId(String sessionId);
}
