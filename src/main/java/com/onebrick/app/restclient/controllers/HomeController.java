package com.onebrick.app.restclient.controllers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.onebrick.app.restclient.entities.UserAccessMapping;
import com.onebrick.app.restclient.models.Transaction;
import com.onebrick.app.restclient.repositories.AuthRepository;
import com.onebrick.app.restclient.repositories.UserAccessMappingRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.text.WordUtils;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

@Controller
@CrossOrigin
public class HomeController {

    @Autowired
    private AuthRepository authRepository;
    @Autowired
    private UserAccessMappingRepository userAccessMappingRepository;
    private ObjectMapper mapper;
    private static final String BASE_URL = "http://dev.onebrick.asia/";
    //private static final String BASE_URL = "http://localhost:8080/";
    //private static final String REDIRECT = "https://demo.onebrick.io/result";
    private static final String REDIRECT = "http://localhost:8085/result";

    @RequestMapping(value = "/ping", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ResponseEntity<String> ping() {
        return ResponseEntity.ok("Server is up and running");
    }

    @RequestMapping("/")
    public String index(Model model) throws Exception {
        var token = authRepository.getPublicToken();
        model.addAttribute("server", BASE_URL + "v1/index");
        model.addAttribute("token", token.get("access_token").textValue());
        model.addAttribute("redirect", REDIRECT);
        return "start";
    }

    @RequestMapping(value = "/result", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity<String> postResult(@RequestBody UserAccess userAccess) throws Exception {
        var randomString = RandomStringUtils.randomAlphanumeric(20);
        var newMapping = new UserAccessMapping();
        newMapping.setSessionId(randomString);
        newMapping.setUserAccessToken(userAccess.getAccessToken());
        if(userAccess.getTransactions() != null) {
            newMapping.setJenius(true);
            newMapping.setTransactions(userAccess.getTransactions());
        }
        userAccessMappingRepository.save(newMapping);
        var url = REDIRECT + "?sessionId=" + randomString;
        return new ResponseEntity<>(url, HttpStatus.OK);
    }

    @RequestMapping(value = "/result", method = RequestMethod.GET)
    public String result(@RequestParam String sessionId, Model model) throws Exception {
        List<Transaction> transactions;
        Balance balance;
        UserAccountDetail detail;
        var session = userAccessMappingRepository.findBySessionId(sessionId);
        var token = authRepository.getPublicToken();

        if(session.isJenius()) {
            transactions = scaffoldJeniusTransactions(session.getTransactions());
            detail = new UserAccountDetail(); //scaffoldJeniusAccounts(session.getAccounts()).get(0);
            balance = new Balance();
            balance.setAvailableBalance(detail.getBalances().getAvailable());
            balance.setCurrentBalance(detail.getBalances().getCurrent());
            balance.setCurrency("IDR");
            balance.setTimestamp(Timestamp.from(Instant.now()));
        } else {
            transactions = getMockTransaction(session.getUserAccessToken());
            balance = getMockBalance(session.getUserAccessToken());
            detail = getMockUserDetail(session.getUserAccessToken());
        }
        detail.setAccountHolder(WordUtils.capitalizeFully(detail.getAccountHolder()));
        model.addAttribute("items", transactions);
        model.addAttribute("balance", balance);
        model.addAttribute("detail", detail);
        model.addAttribute("server", BASE_URL + "v1/index");
        model.addAttribute("token", token.get("access_token").textValue());
        model.addAttribute("redirect", REDIRECT);
        return "result";
    }

    private List<Transaction> getMockTransaction(String userToken) throws Exception {
        var mapper = new ObjectMapper();
        var startDate = setStartDate(new Date());
        var endDate = parseDate(new Date());
        var url = BASE_URL + "v1/transaction/list?from=" + startDate +"&to=" + endDate;
        var client = HttpClientBuilder.create().build();
        var request = new HttpGet(url);
        request.setHeader("Authorization", "Bearer " + userToken);
        var response = client.execute(request);
        var node = mapper.readValue(response.getEntity().getContent(), ObjectNode.class);
        return mapper.convertValue(node.get("data"), new TypeReference<ArrayList<Transaction>>(){});
    }

    private Balance getMockBalance(String userToken) throws Exception {
        var mapper = new ObjectMapper();
        var url = BASE_URL + "v1/account/balance";
        var client = HttpClientBuilder.create().build();
        var request = new HttpGet(url);
        request.setHeader("Authorization", "Bearer " + userToken);
        var response = client.execute(request);
        var node = mapper.readValue(response.getEntity().getContent(), ObjectNode.class);
        return mapper.convertValue(node.get("data"), Balance.class);
    }

    private UserAccountDetail getMockUserDetail(String userToken) throws Exception {
        var mapper = new ObjectMapper();
        var url = BASE_URL + "v1/account/list";
        var client = HttpClientBuilder.create().build();
        var request = new HttpGet(url);
        request.setHeader("Authorization", "Bearer " + userToken);
        var response = client.execute(request);
        var node = mapper.readValue(response.getEntity().getContent(), ObjectNode.class);
        var accounts = mapper.convertValue(node.get("data"), UserAccountDetail[].class);
        if(accounts.length == 0) {
            return new UserAccountDetail();
        } else {
            return accounts[0];
        }
    }

    private String parseDate(Date date) {
        var df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(date);
    }

    private String setStartDate(Date date) {
        var cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);
        return parseDate(cal.getTime());
    }

    private List<Transaction> scaffoldJeniusTransactions(String transactions) throws Exception {
        mapper = new ObjectMapper();
        List<Transaction> result;
        try {
            result = Arrays.asList(mapper.readValue(transactions, Transaction[].class));
            return result;
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    private List<UserAccountDetail> scaffoldJeniusAccounts(String accounts) throws Exception {
        mapper = new ObjectMapper();
        List<UserAccountDetail> result;
        try {
            result = Arrays.asList(mapper.readValue(accounts, UserAccountDetail[].class));
            return result;
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }
}

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
class Balance {
    private String currency;
    private double availableBalance;
    private double currentBalance;
    private Timestamp timestamp;
}

@Data
@NoArgsConstructor
class UserAccess {
    private String accessToken;
    private String transactions;
    //private String accounts;
}

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
class UserAccountDetail {
    private String accountId;
    private String accountHolder;
    private String accountNumber;
    private BalanceDetail balances;

    UserAccountDetail() {
        accountHolder = "John Doe";
        accountNumber = "987-0675-789";
        accountId = "qwerty//++--==";
        balances = new BalanceDetail(100987.0, 100987.0);
    }
}

@Data
@NoArgsConstructor
@AllArgsConstructor
class BalanceDetail {
    private double available;
    private double current;
}
